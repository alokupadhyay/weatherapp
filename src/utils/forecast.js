const request = require('request')

const forecast = (latitude, longitude, callback) => {
    const url = 'http://api.weatherstack.com/current?access_key=9405671b8c1abd9647e4b0591389b12f&query=' + latitude + ',' + longitude

    request({ url, json: true }, (error, { body }) => {
        console.log(body.current.precip);
        if (error) {
            callback('Unable to connect to weather service!', undefined)
        } else if (body.error) {
            callback('Unable to find location', undefined)
        } else {
            callback(undefined, ' temperature :' + body.current.temperature + '  degrees')
        }
    })
}

module.exports = forecast