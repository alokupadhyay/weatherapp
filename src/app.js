const path = require('path')
const express = require('express');
const hbs = require('hbs')
var cors = require('cors');
const { request, response } = require('express');
const app = express()
app.use(cors())
const geocode = require('./utils/geocode')
const forecast = require('./utils/forecast');
const { error } = require('console');


const publicDirectory = path.join(__dirname, '../public');
const viewsPath = path.join(__dirname, './templates/views');
const partialsPath = path.join(__dirname, './templates/partials')


app.set('view engine', 'hbs')
app.set('views', viewsPath)
hbs.registerPartials(partialsPath)
app.use(express.static(publicDirectory))

app.get('/weather', (request, response) => {
  const query = request.query.address
  if (!query) {
    return response.send({
      error: "opps !! its an error"
    })
  }
  // response.send({
  //   title: "Weather Information",
  //   address: geocode
  // })
  geocode(request.query.address, (err, {longitude, location, latitude } = {}) => {
    if (err) {
      return response.send({ err })
    }

    forecast(latitude, longitude, (err, forecastData) => {
      if (err) {
        return response.send({ err })
      }
      response.send({
        forecastData: forecastData,
        location,
        address: query
      })
    })

  })
})

app.get('', (request, response) => {
  response.render('index', {
    title: "Me",
    age: 21
  });
})

app.get('/about', (request, response) => {
  response.render('about', {
    name: "Me",
    age: 21
  });
})

app.get('/help', (request, response) => {
  response.render('help', {
    title: "help page"
  })
})


app.get('*', (request, response) => {
  response.send('its dummy')
})

app.listen(3000, () => {
  console.log("server is up to 3000");
}) 