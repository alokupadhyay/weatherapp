console.log("client side file is loaded!!");
fetch('https://puzzle.mead.io/puzzle').then((response) => {
  response.json().then((data) => {
    console.log(data);
  })
})
// fetch('http://localhost:3000/weather?address=!').then((response) => {
//   response.json().then((data) => {
//     if (data.error) {
//       console.log("opps!! it's an error");
//     }
//     else {
//       console.log(data.location);
//       console.log(data.forecastData);
//     }
//   })
// })
const weatherform = document.querySelector('form')
const search = document.querySelector('input')
const message = document.querySelector('#location-id')
const weather = document.querySelector('#weather-id')

weatherform.addEventListener('submit', (event) => {
  event.preventDefault()
  const location = search.value
  fetch('http://localhost:3000/weather?address=' + location).then((response) => {
    response.json().then((data) => {
      if (data.error) {
        message.textContent = data.error
        weather.textContent = data.error
        console.log("opps!! it's an error");
      }
      else {
        message.textContent = data.location;
        weather.textContent = data.forecastData
        console.log(data.location);
        console.log(data.forecastData);
      }
    })
  })

})